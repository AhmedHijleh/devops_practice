FROM openjdk:8
LABEL Ahmed Abu_Hijleh (ahmed.abuhijleh@progressoft.com)
EXPOSE 8090
ARG JarFile=./target/assignment-*.jar 
COPY ${JarFile} /app/app.jar
ENTRYPOINT java -jar -Dspring.profiles.active=h2 /app/app.jar